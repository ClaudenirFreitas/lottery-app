package com.freitas.lottery.validator;

import java.util.List;
import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.freitas.lottery.annotation.NumberConstraint;
import com.freitas.lottery.dto.TicketLineDTO;
import com.freitas.lottery.util.NumberGenerator;

public class NumberValidator implements ConstraintValidator<NumberConstraint, List<TicketLineDTO>> {
	
	@Override
	public void initialize(NumberConstraint constraint) {
	}

	@Override
	public boolean isValid(List<TicketLineDTO> lines, ConstraintValidatorContext cxt) {
		
		if (Objects.isNull(lines)) {
			return true;
		}
		
		return lines.stream()
				    .map(TicketLineDTO::getLine)
				    .flatMap(List::stream)
				    .allMatch(NumberGenerator.VALID_NUMBERS::contains);
	}
	
}