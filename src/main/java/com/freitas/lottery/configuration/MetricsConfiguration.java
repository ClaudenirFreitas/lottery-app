package com.freitas.lottery.configuration;

import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.micrometer.core.instrument.MeterRegistry;

@Configuration
public class MetricsConfiguration {

	@Bean
	public MeterRegistryCustomizer<MeterRegistry> metrics() {
		return registry -> registry.config()
				                   .commonTags("application", "lottery-app");
	}

}