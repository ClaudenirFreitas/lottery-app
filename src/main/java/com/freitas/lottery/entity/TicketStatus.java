package com.freitas.lottery.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

import com.freitas.lottery.exception.InvalidStatusException;

public enum TicketStatus implements Serializable {

	CHECKED, NOT_CHECKED;

	public static TicketStatus get(final String status) {
		return Arrays.stream(values())
				     .filter(s -> Objects.equals(s.name(), status))
				     .findFirst()
				     .orElseThrow(InvalidStatusException::new);
	}
	
}
