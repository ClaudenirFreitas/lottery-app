package com.freitas.lottery.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity(name = "TICKET_LINE")
public class TicketLine {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "FK_TICKET_ID", nullable = false)
	private Ticket ticket;

	@Column(name = "FIRST_NUMBER", nullable = false)
	@NotNull(message = "Required firstNumber")
	private Integer firstNumber;

	@Column(name = "SECOND_NUMBER", nullable = false)
	@NotNull(message = "Required secondNumber")
	private Integer secondNumber;

	@Column(name = "THIRD_NUMBER", nullable = false)
	@NotNull(message = "Required thirdNumber")
	private Integer thirdNumber;

	@Column(name = "STATUS", nullable = false)
	@NotNull(message = "Required status")
	private Integer status;

	public TicketLine() {
		super();
	}

	public TicketLine(Integer firstNumber, Integer secondNumber, Integer thirdNumber) {
		this.firstNumber = firstNumber;
		this.secondNumber = secondNumber;
		this.thirdNumber = thirdNumber;
	}

	public Integer getFirstNumber() {
		return firstNumber;
	}

	public void setFirstNumber(Integer firstNumber) {
		this.firstNumber = firstNumber;
	}

	public Integer getSecondNumber() {
		return secondNumber;
	}

	public void setSecondNumber(Integer secondNumber) {
		this.secondNumber = secondNumber;
	}

	public Integer getThirdNumber() {
		return thirdNumber;
	}

	public void setThirdNumber(Integer thirdNumber) {
		this.thirdNumber = thirdNumber;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "TicketLine [" + firstNumber + ", " + secondNumber + ", " + thirdNumber + "]";
	}

}