package com.freitas.lottery.util;

import java.util.List;
import java.util.Random;

public final class NumberGenerator {

	public static final List<Integer> VALID_NUMBERS = List.of(0, 1, 2);
	public static final String MESSAGE = "Invalid number on line. Should be: 0, 1 or 2!";

	private static final Random RANDOM = new Random();

	public static final int random() {
		return VALID_NUMBERS.get(RANDOM.nextInt(VALID_NUMBERS.size()));
	}

}
