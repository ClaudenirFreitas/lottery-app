package com.freitas.lottery.util;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.freitas.lottery.dto.TicketDTO;
import com.freitas.lottery.dto.TicketLineDTO;
import com.freitas.lottery.entity.Ticket;
import com.freitas.lottery.entity.TicketLine;

@Component
public class ParseUtil {

	public TicketDTO toTicketDTO(final Ticket ticket) {
		List<TicketLineDTO> lines = ticket.getLines()
				                          .stream()
				                          .map(ticketLine -> 
				                          		new TicketLineDTO(
				                          				ticketLine.getFirstNumber(),
				                          				ticketLine.getSecondNumber(),
				                          				ticketLine.getThirdNumber())
				                          ).collect(Collectors.toList());

		TicketDTO dto = new TicketDTO();
		dto.setId(ticket.getId());
		dto.setLines(lines);
		return dto;
	}

	public Ticket toTicketEntity(final TicketDTO ticketDTO) {

		Ticket ticket = new Ticket();
		ticket.setId(ticketDTO.getId());

		ticket.setLines(ticketDTO.getLines()
                                 .stream()
                                 .map(ticketLineDTO -> toTicketLineEntity(ticket, ticketLineDTO))
                                 .collect(Collectors.toList()));

		return ticket;
	}

	public TicketLine toTicketLineEntity(final TicketLineDTO dto) {
		TicketLine entity = new TicketLine();
		entity.setFirstNumber(dto.getLine().get(0));
		entity.setSecondNumber(dto.getLine().get(1));
		entity.setThirdNumber(dto.getLine().get(2));
		return entity;
	}

	private TicketLine toTicketLineEntity(final Ticket ticket, final TicketLineDTO dto) {
		TicketLine entity = toTicketLineEntity(dto);
		entity.setTicket(ticket);
		return entity;
	}

}