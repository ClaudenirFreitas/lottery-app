package com.freitas.lottery.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.freitas.lottery.annotation.NumberConstraint;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class TicketDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Positive
	@JsonProperty(access = Access.READ_ONLY)
	private Long id;

	@NumberConstraint
	private List<TicketLineDTO> lines;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<TicketLineDTO> getLines() {
		if (Objects.isNull(lines)) {
			lines = new ArrayList<>();
		}
		return lines;
	}

	public void setLines(List<TicketLineDTO> lines) {
		this.lines = lines;
	}

}
