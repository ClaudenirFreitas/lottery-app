package com.freitas.lottery.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.validation.constraints.Size;

public final class TicketLineDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Size(max = 3)
	private final List<Integer> line;

	public TicketLineDTO(final Integer first, final Integer second, final Integer third) {
		line = new ArrayList<>(3);
		line.add(first);
		line.add(second);
		line.add(third);
	}

	public List<Integer> getLine() {
		return Collections.unmodifiableList(line);
	}

}
