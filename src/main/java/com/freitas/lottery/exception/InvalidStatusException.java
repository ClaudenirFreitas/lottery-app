package com.freitas.lottery.exception;

public class InvalidStatusException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InvalidStatusException() {
		super("Invalid status!");
	}

}