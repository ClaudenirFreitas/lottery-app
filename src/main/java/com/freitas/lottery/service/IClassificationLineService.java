package com.freitas.lottery.service;

import java.util.Comparator;

import com.freitas.lottery.entity.TicketLine;

@FunctionalInterface
public interface IClassificationLineService {
	
	int value(final TicketLine line);
	
	default Comparator<TicketLine> order() {
		return (line1, line2) -> (line1.getStatus() > line2.getStatus()) ? -1
				: ((line1.getStatus() == line2.getStatus()) ? 0 : 1);
	}

}
