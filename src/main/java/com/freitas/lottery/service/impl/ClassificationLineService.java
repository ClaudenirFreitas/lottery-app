package com.freitas.lottery.service.impl;

import org.springframework.stereotype.Service;

import com.freitas.lottery.entity.TicketLine;
import com.freitas.lottery.service.IClassificationLineService;
import com.freitas.lottery.service.Rule;

@Service
public class ClassificationLineService implements IClassificationLineService {

	private final Rule rule;

	public ClassificationLineService() {
		Rule firstNumberIsDiff = new FirstNumberIsDiffThanOthers();
		Rule allNumbersAreEquals = new LineNumbersAreEquals(firstNumberIsDiff);
		Rule lineSumEqualsTwo = new LineSumEqualsTwo(allNumbersAreEquals);
		this.rule = lineSumEqualsTwo;
	}

	@Override
	public int value(TicketLine line) {
		return rule.process(line);
	}

}
