package com.freitas.lottery.service.impl;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.freitas.lottery.entity.Ticket;
import com.freitas.lottery.entity.TicketLine;
import com.freitas.lottery.entity.TicketStatus;
import com.freitas.lottery.exception.TicketNotFoundException;
import com.freitas.lottery.exception.TicketStatusException;
import com.freitas.lottery.repository.TicketRepository;
import com.freitas.lottery.service.IClassificationLineService;
import com.freitas.lottery.service.ITicketService;
import com.freitas.lottery.util.NumberGenerator;

@Service
public class TicketService implements ITicketService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TicketService.class.getName());
	
	private final TicketRepository repository;
	private final IClassificationLineService classificationService;

	public TicketService(final TicketRepository repository,
			             final IClassificationLineService classificationService) {
		this.repository = repository;
		this.classificationService = classificationService;
	}

	@Override
	@Transactional
	public Ticket create() {

		final Random random = new Random();
		int count = random.nextInt(20);
		
		LOGGER.info("Creating ticket with {} lines.", count);
		
		Ticket ticket = new Ticket();
		ticket.setStatus(TicketStatus.NOT_CHECKED);
		List<TicketLine> lines = IntStream.range(0, count)
		                                  .mapToObj(index -> {
		           	                          TicketLine line = new TicketLine();
		           	                          line.setTicket(ticket);
		           	                          line.setFirstNumber(NumberGenerator.random());
		           	                          line.setSecondNumber(NumberGenerator.random());
		           	                          line.setThirdNumber(NumberGenerator.random());
		           	                          
		           	                          int status = classificationService.value(line);
		           	                          
		           	                          line.setStatus(status);
		           	                          return line;
		                                  })
		                                  .sorted(classificationService.order()) 
		                                  .collect(Collectors.toList());
		
		ticket.setLines(lines);

		return repository.save(ticket);
	}

	@Override
	public Ticket find(final Long id) throws TicketNotFoundException {
		
		LOGGER.info("Searching ticket with id: {}.", id);
		
		return repository.findById(id)
				         .orElseThrow(TicketNotFoundException::new);
	}
	
	@Override
	public TicketStatus findStatusById(Long id) {
		
		LOGGER.info("Searching status ticket with id: {}.", id);
		
		return repository.findStatusById(id);
	}

	@Override
	public Page<Ticket> findAll(final Pageable pageable) {
		return repository.findAll(pageable);
	}
	
	@Override
	public void updateStatusById(final Long id, final TicketStatus status) throws TicketNotFoundException, TicketStatusException {
		
		LOGGER.info("Updating ticket with id: {} to status: {}", id, status);
		
		Ticket ticket = find(id);
		if (ticket.getStatus() == TicketStatus.CHECKED) {
			throw new TicketStatusException();
		}
		
		ticket.setStatus(status);
		repository.save(ticket);
		
	}
	
	@Override
	@Transactional
	public void addLinesOnTicketBydId(final Long id, final List<TicketLine> lines) {
		
		LOGGER.info("Add {} lines on ticket with id: {}", lines.size(), id);
		
		Ticket ticket = find(id);
		if (ticket.getStatus() == TicketStatus.CHECKED) {
			throw new TicketStatusException();
		}
		
		lines.stream()
		     .map(line -> {
		    	 int status = classificationService.value(line);
                 line.setStatus(status);
                 line.setTicket(ticket);

		    	 return line;
		     })
		     .sorted(classificationService.order()) 
		     .forEach(ticket.getLines()::add);

	}

}
