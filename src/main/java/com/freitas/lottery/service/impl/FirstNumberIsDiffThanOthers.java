package com.freitas.lottery.service.impl;

import java.util.Objects;

import com.freitas.lottery.entity.TicketLine;
import com.freitas.lottery.service.Rule;

public final class FirstNumberIsDiffThanOthers extends Rule {

	public FirstNumberIsDiffThanOthers() {
		super();
	}

	public FirstNumberIsDiffThanOthers(final Rule nextRule) {
		super(nextRule);
	}
	
	@Override
	public boolean check(final TicketLine line) {
		return Objects.equals(line.getSecondNumber(), line.getThirdNumber()) &&
               !Objects.equals(line.getFirstNumber(), line.getSecondNumber());
	}

	@Override
	public int value() {
		return 1;
	}
	
}