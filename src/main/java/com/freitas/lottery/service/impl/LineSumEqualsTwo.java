package com.freitas.lottery.service.impl;

import com.freitas.lottery.entity.TicketLine;
import com.freitas.lottery.service.Rule;

public final class LineSumEqualsTwo extends Rule {

	public LineSumEqualsTwo() {
		super();
	}

	public LineSumEqualsTwo(final Rule nextRule) {
		super(nextRule);
	}

	@Override
	public boolean check(final TicketLine line) {
		return (line.getFirstNumber() + line.getSecondNumber() + line.getThirdNumber()) == 2;
	}

	@Override
	public int value() {
		return 10;
	}

}
