package com.freitas.lottery.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.freitas.lottery.entity.Ticket;
import com.freitas.lottery.entity.TicketStatus;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

	@Query("SELECT t.status FROM TICKET AS t WHERE t.id = :id")
	TicketStatus findStatusById(final Long id);

}
