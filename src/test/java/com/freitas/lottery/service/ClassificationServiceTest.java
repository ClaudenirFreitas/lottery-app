package com.freitas.lottery.service;

import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.freitas.lottery.entity.TicketLine;
import com.freitas.lottery.service.impl.ClassificationLineService;

@Tag("unit-tests")
class ClassificationServiceTest {

	private final IClassificationLineService service = new ClassificationLineService();

	@MethodSource("provider")
	@ParameterizedTest(name = "Expected: {1} for line: {0}")
	void shouldReturnTheCorrectValue(TicketLine input, int expected) {
		// given
		TicketLine line = input;
		// when
		int output = service.value(line);
		// then
		Assertions.assertEquals(expected, output);
		
	}

	private static Stream<Arguments> provider() {
		return Stream.of(
				Arguments.of(new TicketLine(1, 1, 0), 10),
				Arguments.of(new TicketLine(1, 1, 1), 5),
				Arguments.of(new TicketLine(2, 1, 1), 1),
				Arguments.of(new TicketLine(0, 1, 0), 0));
	}

}
