package com.freitas.lottery.controller;

import java.util.List;

import javax.annotation.PostConstruct;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;

import com.freitas.lottery.dto.TicketDTO;
import com.freitas.lottery.dto.TicketLineDTO;
import com.freitas.lottery.entity.Ticket;
import com.freitas.lottery.entity.TicketStatus;
import com.freitas.lottery.repository.TicketRepository;
import com.freitas.lottery.service.ITicketService;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@Tag("integration-tests")
@ContextConfiguration(initializers = DatabaseInitializer.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TicketControllerPatchTest {

	@LocalServerPort 
	private int port;
	
	@Autowired
	private ITicketService service;
	
	@Autowired
	private TicketRepository repository;
	
	@PostConstruct
    public void init() {
        RestAssured.port = port;
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }
	
	@BeforeEach
	void setup() {
		repository.deleteAll();
	}

	@Test
	void shouldUpdateTicket() {
		
		Ticket ticket = service.create();

		RestAssured.given()
			           .pathParam("id", ticket.getId())
			           .body(TicketStatus.CHECKED.name())
		           .when()
		               .patch("/api/v1/tickets/{id}/status")
		           .then()
		               .statusCode(HttpStatus.OK.value());
		
		Ticket updated = service.find(ticket.getId());
		Assertions.assertEquals(TicketStatus.CHECKED, updated.getStatus());
	}

	@Test
	void shouldReturnBadRequestInvalidStatus() {
		
		Ticket ticket = service.create();
		
		RestAssured.given()
			           .pathParam("id", ticket.getId())
			           .body("INVALID")
		           .when()
		               .patch("/api/v1/tickets/{id}/status")
		           .then()
		               .statusCode(HttpStatus.BAD_REQUEST.value())
		               .contentType(ContentType.JSON)
		               .body("status", Matchers.equalTo(HttpStatus.BAD_REQUEST.name()))
                       .body("message", Matchers.equalTo("Invalid status!"))
                       .body("timestamp", Matchers.notNullValue());
		
		Ticket updated = service.find(ticket.getId());
		Assertions.assertEquals(TicketStatus.NOT_CHECKED, updated.getStatus());
	}

	@Test
	void shouldReturnBAD_REQUESTWhenUpdateTicket() {
		
		Ticket ticket = service.create();
		
		ticket.setStatus(TicketStatus.CHECKED);
		repository.save(ticket);

		RestAssured.given()
			           .pathParam("id", ticket.getId())
			           .body(TicketStatus.CHECKED.name())
		           .when()
		               .patch("/api/v1/tickets/{id}/status")
		           .then()
		               .statusCode(HttpStatus.BAD_REQUEST.value())
		               .body("status", Matchers.equalTo(HttpStatus.BAD_REQUEST.name()))
                       .body("message", Matchers.equalTo("Ticket with status CHECKED!"))
                       .body("timestamp", Matchers.notNullValue());
		
		Ticket updated = service.find(ticket.getId());
		Assertions.assertEquals(TicketStatus.CHECKED, updated.getStatus());
	}

	@Test
	void shouldReturnOkWhenAddLinesOnTicket() {
		
		Ticket ticket = service.create();
		
		TicketDTO ticketDTO = new TicketDTO();
		ticketDTO.setLines(List.of(new TicketLineDTO(0, 0, 1),
                                   new TicketLineDTO(0, 0, 1)));
		
		int expected = ticket.getLines().size() + ticketDTO.getLines().size();
		
		RestAssured.given()
			           .pathParam("id", ticket.getId())
			           .contentType(ContentType.JSON)
			           .body(ticketDTO)
		           .when()
		               .patch("/api/v1/tickets/{id}")
		           .then()
		               .statusCode(HttpStatus.OK.value());
		
		Ticket updated = service.find(ticket.getId());
		Assertions.assertEquals(TicketStatus.NOT_CHECKED, updated.getStatus());
		Assertions.assertEquals(expected, updated.getLines().size());
	}

	@Test
	void shouldReturnBAD_REQUESTWhenAddLinesOnTicket() {
		
		Ticket ticket = service.create();
		
		ticket.setStatus(TicketStatus.CHECKED);
		repository.save(ticket);

		TicketDTO ticketDTO = new TicketDTO();
		ticketDTO.setLines(List.of(new TicketLineDTO(0, 0, 1),
                                   new TicketLineDTO(0, 0, 1)));
		
		RestAssured.given()
			           .pathParam("id", ticket.getId())
			           .contentType(ContentType.JSON)
			           .body(ticketDTO)
		           .when()
		               .patch("/api/v1/tickets/{id}")
		           .then()
		               .statusCode(HttpStatus.BAD_REQUEST.value())
		               .body("status", Matchers.equalTo(HttpStatus.BAD_REQUEST.name()))
                       .body("message", Matchers.equalTo("Ticket with status CHECKED!"))
                       .body("timestamp", Matchers.notNullValue());
		
		Ticket updated = service.find(ticket.getId());
		Assertions.assertEquals(TicketStatus.CHECKED, updated.getStatus());
		Assertions.assertEquals(ticket.getLines().size(), updated.getLines().size());
	}

	@Test
	void shouldReturnBadRequestWhenCheckedStatusWhenAddLines() {
		
		Ticket ticket = service.create();
		
		ticket.setStatus(TicketStatus.CHECKED);
		repository.save(ticket);

		TicketDTO ticketDTO = new TicketDTO();
		ticketDTO.setLines(List.of(new TicketLineDTO(0, 1, 2),
                                   new TicketLineDTO(0, 2, 2)));
		
		RestAssured.given()
			           .pathParam("id", ticket.getId())
			           .contentType(ContentType.JSON)
			           .body(ticketDTO)
		           .when()
		               .patch("/api/v1/tickets/{id}")
		           .then()
		               .statusCode(HttpStatus.BAD_REQUEST.value())
		               .body("status", Matchers.equalTo(HttpStatus.BAD_REQUEST.name()))
                       .body("message", Matchers.equalTo("Ticket with status CHECKED!"))
                       .body("timestamp", Matchers.notNullValue());
		
		Ticket updated = service.find(ticket.getId());
		Assertions.assertEquals(TicketStatus.CHECKED, updated.getStatus());
		Assertions.assertEquals(ticket.getLines().size(), updated.getLines().size());
	}

	@Test
	void shouldReturnBadRequestWhenInvalidLine() {
		
		Ticket ticket = service.create();
		
		ticket.setStatus(TicketStatus.CHECKED);
		repository.save(ticket);

		TicketDTO ticketDTO = new TicketDTO();
		ticketDTO.setLines(List.of(new TicketLineDTO(3, 3, 3),
                                   new TicketLineDTO(4, 4, 4)));
		
		RestAssured.given()
			           .pathParam("id", ticket.getId())
			           .contentType(ContentType.JSON)
			           .body(ticketDTO)
		           .when()
		               .patch("/api/v1/tickets/{id}")
		           .then()
		               .statusCode(HttpStatus.BAD_REQUEST.value())
		               .body("status", Matchers.equalTo(HttpStatus.BAD_REQUEST.name()))
                       .body("message", Matchers.equalTo("Invalid number on line. Should be: 0, 1 or 2!"))
                       .body("timestamp", Matchers.notNullValue());
		
		Ticket updated = service.find(ticket.getId());
		Assertions.assertEquals(TicketStatus.CHECKED, updated.getStatus());
		Assertions.assertEquals(ticket.getLines().size(), updated.getLines().size());
	}

	@Test
	void shouldReturnBadRequestWhenInvalidRequestBody() {
		
		Ticket ticket = service.create();

		RestAssured.given()
			           .pathParam("id", ticket.getId())
			           .contentType(ContentType.JSON)
			           .body("{")
		           .when()
		               .patch("/api/v1/tickets/{id}")
		           .then()
		               .statusCode(HttpStatus.BAD_REQUEST.value())
		               .body("status", Matchers.equalTo(HttpStatus.BAD_REQUEST.name()))
                       .body("message", Matchers.equalTo("Invalid format!")) 
                       .body("timestamp", Matchers.notNullValue());

	}

}