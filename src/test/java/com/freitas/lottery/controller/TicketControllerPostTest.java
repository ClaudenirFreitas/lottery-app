package com.freitas.lottery.controller;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;

import com.freitas.lottery.entity.Ticket;
import com.freitas.lottery.entity.TicketStatus;
import com.freitas.lottery.repository.TicketRepository;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@Tag("integration-tests")
@ContextConfiguration(initializers = DatabaseInitializer.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TicketControllerPostTest {

	@LocalServerPort 
	private int port;
	
	@Autowired
	private TicketRepository repository;
	
	@PostConstruct
    public void init() {
        RestAssured.port = port;
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }
	
	@BeforeEach
	void setup() {
		repository.deleteAll();
	}

	@Test
	void shouldReturnCreated() {
		long id = RestAssured.given()
                             .when()
                                 .post("/api/v1/tickets")
                             .then()
                                 .statusCode(HttpStatus.CREATED.value())
                                 .header(HttpHeaders.LOCATION, Matchers.anything("/api/v1/tickets/"))
                                 .contentType(ContentType.JSON)
                                 .body("id", Matchers.notNullValue())
		                         .body("lines.size()", Matchers.not(Matchers.emptyIterable()))
		                     .and()
		                         .extract()
		                         .body()
		                         .jsonPath()
		                         .getLong("id"); 
		
		Optional<Ticket> ticket = repository.findById(id);
		Assertions.assertTrue(ticket.isPresent());
		Assertions.assertEquals(TicketStatus.NOT_CHECKED, ticket.get().getStatus());
		Assertions.assertFalse(ticket.get().getLines().isEmpty());
	}

}