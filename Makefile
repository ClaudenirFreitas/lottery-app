start:
	docker-compose -f docker/docker-compose.yml up -d

stop:
	docker-compose -f docker/docker-compose.yml stop lottery-app lottery-db prometheus grafana 

start-infra:
	docker-compose -f docker/docker-compose.yml up -d lottery-db prometheus grafana

start-app:
	docker-compose -f docker/docker-compose.yml up -d lottery-app

APP_NAME=$$(mvn help:evaluate -Dexpression=project.artifactId -q -DforceStdout)
VERSION:=$$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)
generate-image:
	mvn clean package -DskipTests
	docker build --tag ${APP_NAME}:${VERSION} .
