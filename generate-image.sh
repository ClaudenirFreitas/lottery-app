#/bin/bash

mvn clean package $1

APP_NAME=$(mvn help:evaluate -Dexpression=project.artifactId -q -DforceStdout)
VERSION=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)

docker build --tag $APP_NAME:$VERSION .
